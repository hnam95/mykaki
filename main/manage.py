#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""

import os
import sys
from django.core.management import execute_from_command_line

setting_module = os.environ.get("DJANGO_SETTINGS_MODULE")
if not setting_module:
    raise Exception("DJANGO_SETTINGS_MODULE is not in environment variable.")


def main():
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
