from django.contrib import admin
from django.urls import include, path
from rest_framework import routers

from main.views.external_normal_shipment_view import ExternalNormalShipmentView

API_VERSION = 3
external_router = routers.SimpleRouter(trailing_slash=False)
external_router.register('normal-shipments', ExternalNormalShipmentView)
# external_router.register('pooling-shipments', PoolingShipmentView)


EXTERNAL_APIS = [
    path('', include(external_router.urls)),
]

INTERNAL_APIS = [

]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v%s/' % API_VERSION, include(EXTERNAL_APIS)),
    path('api/v%s/internal/' % API_VERSION, include(INTERNAL_APIS)),
]
