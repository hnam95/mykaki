import configparser
import os
from pathlib import Path

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        validate_config()


def validate_config():
    """
    Validate config.ini file
    """
    config = configparser.ConfigParser()
    path = Path(__file__).parent.parent.parent
    config.read(os.path.join(path, 'config.ini'))
    print(config['EMAIL'])

    required_config = {
        'DEBUG': [
            'DEBUG'
        ],
        'SECRET': [
            'DB_PASSWORD',
        ],
        'EMAIL': [
            'LIAISING_EMAIL_ADDRESS',
        ]
    }

    for session in required_config:
        if session not in config:
            raise Exception("Config missing session: %s" % session)
        for key in required_config[session]:
            if key not in config[session] or not config[session][key]:
                raise Exception("Config missing: %s" % key)
