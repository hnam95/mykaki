from main.settings.common import *

DEBUG = True

LIAISING_EMAIL_ADDRESS = config['EMAIL']['LIAISING_EMAIL_ADDRESS']

DATABASES = {
   'default': {
       'ENGINE': 'django.db.backends.postgresql',
       'NAME': 'local_db',
       'USER': 'mykaki',
       'PASSWORD': '12345678',
       'HOST': 'localhost',
       'PORT': '5432',
   }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'console': {
            'format': '%(filename)s:%(lineno)s %(funcName)s:\n  %(message)s',
        },
        'file': {
            'format': '%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'console',
        },
    },
    'loggers': {
        'internal': {
            'handlers': ['console'],
            'level': 'DEBUG'
        },
        'external': {
            'handlers': ['console'],
            'level': 'DEBUG'
        },
    },
}
