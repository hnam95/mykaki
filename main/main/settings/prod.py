from main.settings.common import *

DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'master_db',
        'USER': 'swdigital',
        'PASSWORD': config['SECRET']['DB_PASSWORD'],
        'HOST': '35.247.172.147',
        'PORT': '5432',
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': "[%(asctime)s] %(levelname)s %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'file_django': {
            'formatter': 'verbose',
            'level': 'INFO',
            'filename': '/home/swdigitalco/log/django/log',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'when': 'midnight',
            'interval': 1,
        },
        'file_django_db': {
            'formatter': 'verbose',
            'level': 'INFO',
            'utc': True,
            'filename': '/home/swdigitalco/log/db/log',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'when': 'midnight',
            'interval': 1,
        },
        'file_custom': {
            'formatter': 'verbose',
            'level': 'INFO',
            'filename': '/home/swdigitalco/log/custom/log',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'when': 'midnight',
            'interval': 1,
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file_django'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'django.db': {
            'handlers': ['file_django_db'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'freightknot': {
            'handlers': ['file_custom'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },
}
