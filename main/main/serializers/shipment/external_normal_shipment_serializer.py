from drf_dynamic_fields import DynamicFieldsMixin
from rest_framework import serializers

from main.models.shipment_model import ShipmentModel


class ExternalCreateNormalShipmentSerializer(DynamicFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = ShipmentModel
        fields = [
            'shipment_id'
        ]

    # def to_representation(self, instance):
    #     return ExternalRetrieveShipmentSerializer(instance).data
