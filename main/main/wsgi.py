import configparser
import os

from django.core.wsgi import get_wsgi_application


django_setting_module = os.environ.get('DJANGO_SETTINGS_MODULE')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', django_setting_module)

application = get_wsgi_application()
