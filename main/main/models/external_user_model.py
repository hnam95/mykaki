from django.db import models


class ExternalUserModel(models.Model):
    consignee_id = models.TextField(unique=True)
    email = models.EmailField()
    business_uen = models.TextField(blank=True)
    password = models.TextField()
    account_status = models.TextField()
    is_business_account = models.BooleanField()
    company_name = models.TextField(blank=True)
    name = models.TextField()
    avatar = models.TextField(null=True)
    phone = models.TextField(null=True, blank=True)
    last_login_at = models.DateTimeField(auto_now_add=True)
    fail_login_count = models.IntegerField(default=0)
    is_deleted = models.BooleanField(default=False)
    visited_dashboard = models.BooleanField(default=False)

    # Provided UEN or/and ID photo then confirmed by internal user
    is_verified = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    member_emails = models.TextField(default="")
    stripe_customer_id = models.TextField(default='')

    vid = models.TextField(default='')

    class Meta:
        db_table = "external_user"
