from django.db import models


class FinalQuoteModel(models.Model):
    calculation_method = models.TextField(default='cbm')
    final_rate = models.FloatField(default=0)

    actual_freight_cost = models.FloatField(default=0)

    vgm_charge = models.FloatField(default=0)
    vgm_charge_note = models.TextField(default='', blank=True)

    fumigation_charge = models.FloatField(default=0)
    fumigation_charge_note = models.TextField(default='', blank=True)

    origin_extra_fee = models.FloatField(default=0)
    origin_extra_fee_note = models.TextField(default='', blank=True)

    collection_cost = models.FloatField(default=0)
    collection_cost_note = models.TextField(default='', blank=True)

    delivery_cost = models.FloatField(default=0)
    delivery_cost_note = models.TextField(default='', blank=True)

    cargo_discrepancy_charge = models.FloatField(default=0)
    cargo_discrepancy_charge_note = models.TextField(default='', blank=True)

    miss_loading_charge = models.FloatField(default=0)
    miss_loading_charge_note = models.TextField(default='', blank=True)

    transport_fee = models.FloatField(default=0)
    transport_fee_note = models.TextField(default='', blank=True)

    origin_storage_fee = models.FloatField(default=0)
    origin_storage_fee_note = models.TextField(default='', blank=True)

    special_size_fee = models.FloatField(default=0)
    special_size_fee_note = models.TextField(default='', blank=True)

    origin_custom_penalty = models.FloatField(default=0)
    origin_custom_penalty_note = models.TextField(default='', blank=True)

    actual_goods_type = models.TextField(default='general')
    currency = models.TextField(default='SGD')
    final_quote = models.FloatField(default=0)

    created_at = models.DateTimeField(auto_now_add=True)
    sent_by = models.TextField(default='')

    class Meta:
        db_table = "final_quote"
