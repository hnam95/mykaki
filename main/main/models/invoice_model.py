from django.db import models


class InvoiceModel(models.Model):
    invoice_no = models.TextField()
    payment_status = models.TextField()
    payment_method = models.TextField()
    transferred_amount = models.FloatField(default=0)
    bank_transfer_note = models.TextField(default='', blank=True)
    transfer_time = models.DateTimeField(null=True)
    due_date = models.DateTimeField(default=None, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    stripe_payment_intent_id = models.TextField(default='', blank=True)
    selected_card_id = models.TextField(default='', blank=True)

    class Meta:
        db_table = "invoice"
