import datetime

from django.db import models
from django.utils import timezone

from main.lib.enums.pooling_status import PoolingStatus

from main.models.pooling_range_model import PoolingRangeModel


class PoolingModel(models.Model):
    loading_date = models.DateField()
    name = models.TextField(default='')
    expected_sg_arrive_date = models.DateField()
    latest_collection_date = models.DateField()
    reserved_cbm = models.FloatField(null=True)
    used_cbm = models.FloatField(null=True)
    available_cbm = models.FloatField(null=True)
    pooling_range = models.ForeignKey(PoolingRangeModel, on_delete=models.CASCADE, null=True)
    publish_date = models.DateField(null=True)
    close_booking_date = models.DateField(null=True)
    pooling_status = models.TextField()
    container_type = models.TextField()
    currency = models.TextField(null=True)

    created_by = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_by = models.TextField()
    updated_at = models.DateTimeField(auto_now=True)

    sensitive_rate_xs = models.FloatField()
    general_rate_xs = models.FloatField()

    sensitive_rate_1s = models.FloatField()
    sensitive_rate_2s = models.FloatField()
    sensitive_rate_3s = models.FloatField()
    sensitive_rate_4s = models.FloatField()
    general_rate_1s = models.FloatField()
    general_rate_2s = models.FloatField()
    general_rate_3s = models.FloatField()
    general_rate_4s = models.FloatField()

    sensitive_rate_1m = models.FloatField()
    sensitive_rate_2m = models.FloatField()
    sensitive_rate_3m = models.FloatField()
    sensitive_rate_4m = models.FloatField()
    general_rate_1m = models.FloatField()
    general_rate_2m = models.FloatField()
    general_rate_3m = models.FloatField()
    general_rate_4m = models.FloatField()

    sensitive_rate_1l = models.FloatField()
    sensitive_rate_2l = models.FloatField()
    sensitive_rate_3l = models.FloatField()
    sensitive_rate_4l = models.FloatField()
    general_rate_1l = models.FloatField()
    general_rate_2l = models.FloatField()
    general_rate_3l = models.FloatField()
    general_rate_4l = models.FloatField()

    class Meta:
        db_table = "pooling"

    def __str__(self):
        return "id: %s, loading date: %s" % (self.id, self.loading_date)

    def get_rate(self, cbm, is_sensitive=False, time=timezone.now().date()):
        """Get rate at specific time, default = None.
        """
        size = ''
        rate_type = 'sensitive' if is_sensitive else 'general'
        pooling_range_index = self.pooling_range.get_range(time)
        if cbm < 0.5:
            size = 'xs'
            pooling_range_index = ''

        if 0.5 <= cbm < 1:
            size = 's'

        if 1 <= cbm < 3:
            size = 'm'

        if cbm >= 3:
            size = 'l'

        rate = getattr(self, '%s_rate_%s%s' % (rate_type, pooling_range_index, size))

        return rate

    def get_rate_on_currency(self, currency, *args, **kwargs):
        from freightknot.services.pricing_service import PricingService

        default_rate = self.get_rate(*args, **kwargs)
        foreign_rate = PricingService.convert_currency(default_rate, self.currency, currency)
        return foreign_rate

    def publish(self):
        self.pooling_status = PoolingStatus.PUBLISHED.value
        self.save()

    def close(self):
        self.pooling_status = PoolingStatus.CLOSED.value
        self.save()

    def get_current_range(self):
        """Return an integer represents the current range of this loading date.
        """
        time = datetime.datetime.today().date()
        pooling_range = self.pooling_range
        if time <= pooling_range.start_time_range_1:
            return 0
        if time > pooling_range.start_time_range_1:
            return 1
        if time > pooling_range.start_time_range_2:
            return 2
        if time > pooling_range.start_time_range_3:
            return 3
        if time > pooling_range.start_time_range_4:
            return 4
        if time > pooling_range.end_time_range_2:
            return 5

    def get_next_change_rate_time(self):
        current_range = self.get_current_range()
        next_change_rate = getattr(self.pooling_range, "start_time_range_%s" % str(current_range+1))
        return next_change_rate
