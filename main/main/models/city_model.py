from django.db import models


class CityModel(models.Model):
    city_name = models.TextField(default='')
    city_code = models.TextField(default='')
    country_name = models.TextField(default='')
    country_code = models.TextField(default='')
