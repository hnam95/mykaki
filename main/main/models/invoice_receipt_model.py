from django.db import models

from main.models.invoice_model import InvoiceModel


class InvoiceReceiptModel(models.Model):
    uploaded_by = models.TextField()
    uploaded_user_type = models.TextField(default='external')
    public_url = models.URLField()
    invoice = models.ForeignKey(InvoiceModel, on_delete=models.CASCADE, related_name='receipts')

    class Meta:
        db_table = "invoice_receipt"
