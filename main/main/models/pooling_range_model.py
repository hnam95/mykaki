from django.db import models
from django.utils import timezone


class PoolingRangeModel(models.Model):
    start_time_range_1 = models.DateField()
    end_time_range_1 = models.DateField()
    start_time_range_2 = models.DateField()
    end_time_range_2 = models.DateField()
    start_time_range_3 = models.DateField()
    end_time_range_3 = models.DateField()
    start_time_range_4 = models.DateField()
    end_time_range_4 = models.DateField()

    class Meta:
        db_table = "pooling_range"

    def get_range(self, time=timezone.now().date()):
        """Return an integer represents the index of range time.
        """
        if time < self.start_time_range_1:
            return 0
        elif time < self.start_time_range_2:
            return 1
        elif time < self.start_time_range_3:
            return 2
        elif time < self.start_time_range_4:
            return 3
        return 4
