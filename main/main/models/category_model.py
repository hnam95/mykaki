from django.db import models

from main.models.shipment_model import ShipmentModel


class CategoryModel(models.Model):
    shipment = models.ForeignKey(ShipmentModel, on_delete=models.CASCADE, to_field='shipment_id')
    name = models.TextField()

    class Meta:
        db_table = "category"
