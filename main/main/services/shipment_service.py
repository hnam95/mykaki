@staticmethod
def convert_currency(amount, from_currency, to_currency):
    from freightknot.models.currency_rate import CurrencyRateModel

    from_rate = CurrencyRateModel.objects.get(currency_code=from_currency).currency_rate
    to_rate = CurrencyRateModel.objects.get(currency_code=to_currency).currency_rate

    sgd_amount = amount * from_rate
    to_amount = sgd_amount / to_rate
    return to_amount
