import datetime
from datetime import timedelta

from rest_framework import exceptions
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from main.lib.authentication.authentication_classes import ExternalUserTokenAuthentication
from main.lib.enums.shipment_type import ShipmentType
from main.lib.pagination import StandardResultsSetPagination
from main.models.shipment_model import ShipmentModel
from main.serializers.shipment.external_normal_shipment_serializer import ExternalCreateNormalShipmentSerializer


class ExternalNormalShipmentView(ModelViewSet):
    authentication_classes = [ExternalUserTokenAuthentication]
    queryset = ShipmentModel.objects.filter(shipment_type=ShipmentType.NORMAL.value,
                                            is_invalid=False).order_by("-shipment_id")
    # filter_backends = [ExternalListShipmentFilterBackend]
    lookup_field = "shipment_id"
    pagination_class = StandardResultsSetPagination

    def get_serializer_class(self):
        return ExternalCreateNormalShipmentSerializer
